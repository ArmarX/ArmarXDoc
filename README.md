# ArmarXDoc

This project is used to build the ArmarX online API documentation,
which is available at https://armarx.humanoids.kit.edu/.

See also the [ArmarX Academy](https://gitlab.com/ArmarX/meta/Academy).


## Installation via Axii

```shell
axii add armarx/meta/ArmarXDoc
```


## Build the Documentation Locally

You can build the documentation locally:

```shell
cd build/
cmake ..
make doc
```

This will create files for an HTML website in `build/doxygen/html/`.
You can open the website locally, e.g. with Firefox:
```shell
cd build/
firefox doxygen/html/index.html
```
